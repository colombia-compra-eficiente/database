CREATE TABLESPACE colombia_compra_eficiente 
   DATAFILE 'colombia_compra_eficiente.dbf' 
   SIZE 1000M;
   
ALTER SESSION SET “_ORACLE_SCRIPT” = TRUE;
   
CREATE USER prueba_tecnica
  IDENTIFIED BY Pru3b4_T
  DEFAULT TABLESPACE colombia_compra_eficiente
  QUOTA 1000M on colombia_compra_eficiente;
  
GRANT create session TO prueba_tecnica;
GRANT create table TO prueba_tecnica;
GRANT create view TO prueba_tecnica;
GRANT create any trigger TO prueba_tecnica;
GRANT create any procedure TO prueba_tecnica;
GRANT create sequence TO prueba_tecnica;
GRANT create synonym TO prueba_tecnica;