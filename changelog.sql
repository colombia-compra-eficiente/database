--liquibase formatted sql


--changeset kevin.jamaica:1 labels:create-sequence
--comment: creation of client sequence
create sequence sec_client start with 1 increment by 1 nocache nocycle;
--rollback DROP SEQUENCE sec_client;


--changeset kevin.jamaica:2 labels:create-sequence
--comment: creation of product sequence
create sequence sec_product start with 1 increment by 1 nocache nocycle;
--rollback DROP SEQUENCE sec_product;


--changeset kevin.jamaica:3 labels:create-sequence
--comment: creation of shopping_cart sequence
create sequence sec_shopping_cart start with 1 increment by 1 nocache nocycle;
--rollback DROP SEQUENCE sec_shopping_cart;


--changeset kevin.jamaica:4 labels:create-table
--comment: creation of client table
create table client (
    id number default sec_client.nextval not null,
    name varchar2(50) not null,
    email varchar2(50) not null,
    password varchar2(50) not null,
	enabled number(1, 0) default 1 not null,
	creation_date timestamp default sysdate not null,
	update_date timestamp
)
--rollback DROP TABLE client;


--changeset kevin.jamaica:5 labels:create-table
--comment: creation of product table
create table product (
    id number default sec_product.nextval not null,
    name varchar2(50) not null,
    stock number not null,
    price number(4, 2),
	enabled number(1, 0) default 1 not null,
	creation_date timestamp default sysdate not null,
	update_date timestamp
)
--rollback DROP TABLE product;


--changeset kevin.jamaica:6 labels:create-table
--comment: creation of shopping_cart table
create table shopping_cart (
    id number default sec_shopping_cart.nextval not null,
    client_id number not null,
	product_id number not null, 
	amount number,
	paid number(1, 0) default 0 not null,
	enabled number(1, 0) default 1 not null,
	creation_date timestamp default sysdate not null,
	update_date timestamp
)
--rollback DROP TABLE shopping_cart;


--changeset kevin.jamaica:7 labels:create-pk
--comment: creation of client pk
alter table client add constraint client_pk primary key ( id );


--changeset kevin.jamaica:8 labels:create-pk
--comment: creation of product pk
alter table product add constraint product_pk primary key ( id );


--changeset kevin.jamaica:9 labels:create-pk
--comment: creation of shopping_cart pk
alter table shopping_cart add constraint shopping_cart_pk primary key ( id );


--changeset kevin.jamaica:10 labels:create-fk
--comment: creation of shopping_cart fk to client
alter table shopping_cart add constraint fk_shopping_cart_client foreign key ( client_id ) references client ( id );


--changeset kevin.jamaica:11 labels:create-fk
--comment: creation of shopping_cart fk to product
alter table shopping_cart add constraint fk_shopping_cart_product foreign key ( product_id ) references product ( id );


--changeset kevin.jamaica:12 labels:insert
--comment: insert some clients
insert into client (name, email, password) values ('PEDRO PEREZ', 'pepeperez@gmail.com', '123456');
insert into client (name, email, password) values ('RODRIGO RODRIGUEZ', 'rr_urdaneta@gmail.com', '789012');
insert into client (name, email, password) values ('JIMENA JIMENEZ', 'jjimenezr@gmail.com', '345678');


--changeset kevin.jamaica:13 labels:insert
--comment: insert some products
insert into product (name, stock, price) values ('Leche', 12, 2.00);
insert into product (name, stock, price) values ('Huevo', 20, 0.15);
insert into product (name, stock, price) values ('Arroz', 14, 1.25);
insert into product (name, stock, price) values ('Cereal', 7, 4.50);
insert into product (name, stock, price) values ('Jamón', 4, 3.15);
