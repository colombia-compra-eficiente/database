# database

Este proyecto contiene la capa de datos para la prueba técnica correspondiente.

## Requisitos

Este proyecto fue creado con las siguientes herramientas sobre Windows 11:
- Oracle 21c
- Liquibase Community Version 4.10.0
- Herramienta para gestión de base de datos. Para este proyecto se usó SQL Developer.

## Getting started

Para comenzar, se recomienda ingresar a la base de datos como administrador, para crear el tablespace requerido y el usuario con el que se va a crear la base de datos. 

El script para la generación del tablespace, el usuario con el que se va conectar el backend y los permisos, se encuentra en database/creation.sql.


Una vez creado el usuario, se procede a abrir una ventana de comandos CMD y se navega hasta donde se encuentre la raíz de este proyecto. Una vez ubicado ahí, se debe ejecutar el comando:
```
liquibase update
```

Se recomienda haber habilitado en el path los comandos de liquibase.

Con la ejecución de este comando, se creará la estructura de base de datos y unos datos de ejemplo.
